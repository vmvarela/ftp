# ftp

Simple FTP server.


## Usage

You use environment variables to change your settings.

	FTP_IPADDRESS=<external IP>
	FTP_USERS=EXAMPLE1
	FTP_EXAMPLE1_USERNAME=<default:uservarname in lowercase, i.e. bob>
	FTP_EXAMPLE1_PASSWORD=<default:username>
	FTP_EXAMPLE1_HOME=<default:/ftp/username>

## Example

	$ sudo docker run -it --rm \
		-p 20:20 -p 21:21 \
		-p 12020:12020 -p 12021:12021 -p 12022:12022 -p 12023:12023 -p 12024:12024 \
		-e FTP_USERS=MYUSER -e FTP_MYUSER_PASSWORD=mypass \
		near/ftp
	
NOTE: Ports 20/tcp and 21/tcp must be redirected, and 12020-12025 to allow passive mode.

	
